
function [STFT, f, t] = stftCore(signal, samplingFrequency, overlapNumber, windowLength, windowType)

% window formation
window = [];
switch windowType
    case 1;  window = hamming(windowLength);
    case 2;  window = kaiser(windowLength, 0.33);
    case 3;  window = triang(windowLength);
    case 4;  window = rectwin(windowLength);
    case 5;  window = hann(windowLength);
end

% stft matrix size estimation and preallocation
frequencyRange = ceil((1+windowLength)/2);
timeRange = 1 + floor((length(signal)-windowLength)/overlapNumber);
STFT = zeros(frequencyRange, timeRange);

% stft calculation
for ind = 0 : timeRange-1
    
    % segmentation
    dx = signal(1 + ind*overlapNumber : windowLength + ind*overlapNumber);
    
    % windowing
    dx  = dx(:).*window;
    
    % FFT
    X = fft(dx, windowLength);
    
    % update of the stft matrix
    STFT(:, 1+ind) = X(1:frequencyRange);
end

% normalisation
STFT = abs(STFT)/windowLength;
STFT = 20*log10(STFT);

% time and frequency scales
t = (windowLength/2 : overlapNumber : windowLength/2+(timeRange-1)*overlapNumber) / samplingFrequency;
f = (0 : frequencyRange-1) * samplingFrequency/windowLength;
