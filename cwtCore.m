function [CWT, f, t] = cwtCore(signal, samplingFrequency, frequencyBandwidth, waveletType)

% determine frequency range
fx = fft(signal, samplingFrequency);
fx = abs(fx(1:end/2));

% wavelet bank definitionan
wavelets = waveletBank(waveletType, samplingFrequency, frequencyBandwidth);

% compute continous wavelet transform
CWT = [];
for ind = 1 : min(size(wavelets))

    CWT(ind, :) = conv(signal, wavelets(ind, :), 'same');
end

% determine time and frequency vectors
f = 1 : samplingFrequency/size(wavelets, 1) : samplingFrequency;
f = f(1:size(CWT, 1));
t = 0 : 1/samplingFrequency : length(signal)/samplingFrequency;
t = t(1:size(CWT, 2));