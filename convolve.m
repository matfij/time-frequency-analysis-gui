
function Z = convolve(x, y)

X = [x, zeros(1, length(y))]; 
Y = [y, zeros(1, length(x))]; 

for i=1:length(y)+length(x)-1
    
    Z(i)=0;
    for j=1:length(x)
        
        if(i-j+1>0)
            Z(i)=Z(i)+X(j)*Y(i-j+1); end
    end
end
