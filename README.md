# Matlab GUI for time-frequency domain analysis

This tool support custom Short time Fourier transform and continuous wavelet transform as well
as other visualizations (spectrogram and scalogram).

## Requirements
Matlab R2019a
Signal Processing Toolbox

## Resources
https://www.youtube.com/watch?v=g1_wcbGUcDY
https://www.youtube.com/watch?v=QX1-xGVFqmw