function wavelets = waveletBank(waveletType, samplingFrequency, frequencyBandwidth)

% wavelet size, count
t = -1 : 0.02: 1;
bankSize = 200;

% freqs
freqs = [];
for ind = 0 : bankSize - 1
    
%     freqs(ind+1) = sqrt((1 + samplingFrequency*(1.25*ind)) / bankSize );
    freqs(ind+1) = 0.275*ind;
end

% scales
scales = 0 : 1/(8*bankSize) : 0.3;
scales = flip(scales);

% wavelets calculation
switch waveletType
    % Morlet
    case 1
        for ind = 1 : bankSize

             wavelets(ind, :) = ((pi*frequencyBandwidth)^(-0.5))*exp(2*1i*pi*freqs(ind)*t).*exp(-(t/scales(ind)).^2/frequencyBandwidth);
        end
    % Shannon
    case 2
        for ind = 1 : bankSize
                
            wavelets(ind, :) = (frequencyBandwidth^0.5)*(sinc(frequencyBandwidth*t/scales(ind)).*exp(2*1i*pi*freqs(ind)*t));
        end  
end


