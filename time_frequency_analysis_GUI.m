function varargout = time_frequency_analysis_GUI(varargin)
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @time_frequency_analysis_GUI_OpeningFcn, ...
                   'gui_OutputFcn',  @time_frequency_analysis_GUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end
if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
function time_frequency_analysis_GUI_OpeningFcn(hObject, eventdata, handles, varargin)
handles.output = hObject;
guidata(hObject, handles);
function varargout = time_frequency_analysis_GUI_OutputFcn(hObject, eventdata, handles)
varargout{1} = handles.output;


% --- Executes on button press in btnPlot.
function btnPlot_Callback(~, ~, handles)

try
    fs = str2double(get(handles.signalFrequency, 'String'));
    time = str2double(get(handles.signalTime, 'String'));
    signal = get(handles.signalFunction, 'String');
    
    t = 0 : 1/fs: time;
    x = eval(signal);

    axes(handles.timeAxes);
    plot(t, x); axis tight
    title('Time Domain'); xlabel('Time [s]'); ylabel('Amplitude');
catch
    warndlg('Please enter correct signal parameters', 'Input Warning')
end


% --- Executes on button press in analyzeBtn.
function analyzeBtn_Callback(~, ~, handles)

% signal data
try
    fs = str2double(get(handles.signalFrequency, 'String'));
    time = str2double(get(handles.signalTime, 'String'));
    signal = get(handles.signalFunction, 'String');
    
    t = 0 : 1/fs: time;
    x = eval(signal);
catch
    warndlg('Please enter correct signal parameters', 'Input Warning')
    return
end

% analysis
fx = fft(x, fs);
axes(handles.frequencyAxes); plot(abs(fx(1:end/2)));
view(90,270); axis tight;
title('Frequency Domain'); xlabel('Frequency [Hz]'); ylabel('Amplitude');

analysisType = get(handles.analysisType, 'Value');

switch analysisType
    
    % STFT
    case 1
        overlapNumber = str2double(get(handles.stftOverlapNumber, 'String'));
        windowLength = str2double(get(handles.stftWindowLength, 'String'));
        windowType = get(handles.stftWindowType, 'Value');
        
        [STFT, f, t] = stftCore(x, fs, overlapNumber, windowLength, windowType);
        
        axes(handles.timeFrequencyAxes); surf(t, f, STFT);
        shading interp; axis tight; view(0, 90);
        title('Spectrogram'); xlabel('Time [s]'); ylabel('Frequency [Hz]');
        hcol = colorbar; ylabel(hcol, 'Magnitude [dB]');
        
    % CWT
    case 2
        frequencyBandwidth = str2double(get(handles.cwtFrequencyBandwidth, 'String'));
        waveletType = get(handles.cwtWaveletType, 'Value');
        
        [CWT, f, t] = cwtCore(x, fs, frequencyBandwidth, waveletType);
        
        axes(handles.timeFrequencyAxes); surf(t, f, abs(CWT));
        shading interp; axis tight; view(0, 90);
        title('Scalogram'); xlabel('Time [s]'); ylabel('Frequency [Hz]');
        hcol = colorbar; ylabel(hcol, 'Magnitude [dB]');

end


% --- Executes on selection change in analysisType.
function analysisType_Callback(~, ~, handles)

analysisType = get(handles.analysisType, 'Value');

switch analysisType
    case 1
        set(handles.cwtPanel, 'visible', 'off')
    case 2
        set(handles.cwtPanel, 'visible', 'on')
end
